#pragma once

#include <controller/Changer.h>

namespace changer {

class FileCollapser : public dependency::Changer {
public:
    ~FileCollapser() override = default;
    void change(dependency::DependencyGraph& graph) const override;
};
}
