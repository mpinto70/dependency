#include <controller/Controller.h>

#include <filesystem>

namespace dependency {

void Controller::run(const std::unique_ptr<Reader>& reader,
      const std::vector<std::unique_ptr<Changer>>& changers,
      const std::unique_ptr<Writer>& writer,
      const std::vector<Highlight>& highlights,
      bool auto_highlight,
      std::ostream& out) {
    auto graph = reader->read();
    for (const auto& changer : changers) {
        changer->change(graph);
    }
    auto full_highlights = auto_highlight ? build_full_highlights(graph, highlights) : highlights;
    writer->write(out, graph, full_highlights);
}

std::vector<Highlight> Controller::build_full_highlights(const DependencyGraph& graph,
      std::vector<Highlight> highlights) {
    std::set<std::string> keys;
    for (const auto& node : graph.nodes()) {
        const std::string key = std::filesystem::path(node).parent_path();
        keys.insert(key + "/");
    }
    size_t count = 0;
    for (auto rit = keys.rbegin(); rit != keys.rend(); ++rit) {
        const auto& color = AUTO_COLORS[count % AUTO_COLORS.size()];
        highlights.push_back({ *rit, color });
        ++count;
    }
    return highlights;
}

}
