#include <controller/Highlight.h>

#include <gtest/gtest.h>
#include <stdexcept>

namespace dependency {

bool operator==(const Highlight& x, const Highlight& y) {
    return x.text == y.text
           && x.color.foreground == y.color.foreground
           && x.color.background == y.color.background;
}

TEST(HighlightTest, AUTO_COLORS) {
    EXPECT_GT(AUTO_COLORS.size(), 0u);
}

TEST(HighlightTest, create) {
    EXPECT_EQ(Highlight::create("text,color1,color2"), (Highlight{ "text", { "color1", "color2" } }));
    EXPECT_EQ(Highlight::create("text"), (Highlight{ "text", { "blue", "lightcyan" } }));
}

TEST(HighlightTest, create_vector) {
    const std::vector<std::string> highlights = {
        "text0,foreground0,background0",
        "text1,foreground1,background1",
        "text2,foreground2,background2",
        "text3,foreground3,background3",
        "text4,foreground4,background4",
    };
    std::vector<Highlight> expected;
    for (const auto& highlight : highlights) {
        expected.push_back(Highlight::create(highlight));
    }
    EXPECT_EQ(Highlight::create(highlights), expected);
}

TEST(HighlightTest, error) {
    EXPECT_THROW(Highlight::create("text,color1"), std::invalid_argument);
    EXPECT_THROW(Highlight::create("text,color1,color2,other"), std::invalid_argument);
}
}
