#include <changer/Cutter.h>
#include <util/Util.h>

#include <gtest/gtest.h>

namespace changer {
using dependency::Changer;
using dependency::DependencyGraph;

// A-->B
// C-->D
// E-->F
TEST(CutterTest, no_cut) {
    DependencyGraph graph;
    graph.add("A", { "B" });
    graph.add("C", { "D" });
    graph.add("E", { "F" });

    const Cutter cutter({ "XYX", "ABC" });
    const Changer& changer = cutter;

    const auto expected = graph;
    changer.change(graph);
    EXPECT_EQ(graph, expected);
}

TEST(CutterTest, cut) {
    using dependency::node_T;
    DependencyGraph graph;
    graph.add("ABC", { "ABCD", "XAB", "CDEF", "AB" });
    graph.add("CDEF", { "CDEFG", "XCD" });
    graph.add("XCD", { "F" });
    graph.add("AB", { "CD" });
    graph.add("CD", { "AB" });
    graph.add("abC", { "abCD", "XAB", "cdEF", "ab" });
    DependencyGraph expected = graph;
    expected.change([=](const node_T& node) {
        if (node.size() > 2 && (util::starts_with(node, "AB") || util::starts_with(node, "CD"))) {
            return node.substr(2);
        } else {
            return node;
        }
    });

    const Cutter cutter({ "AB", "CD" });
    const Changer& changer = cutter;

    changer.change(graph);
    EXPECT_EQ(graph, expected);
}
}
