#pragma once

#include <controller/Changer.h>
#include <controller/Highlight.h>
#include <controller/Reader.h>
#include <controller/Writer.h>

#include <memory>

namespace dependency {

class Controller {
public:
    Controller() = delete;
    ~Controller() = delete;
    static void run(const std::unique_ptr<Reader>& reader,
          const std::vector<std::unique_ptr<Changer>>& changers,
          const std::unique_ptr<Writer>& writer,
          const std::vector<Highlight>& highlights,
          bool auto_highlight,
          std::ostream& out);

    static std::vector<Highlight> build_full_highlights(const DependencyGraph& graph,
          std::vector<Highlight> highlights);
};
}
