#include <changer/DirectoryCollapser.h>

#include <gtest/gtest.h>

namespace changer {
using dependency::Changer;
using dependency::DependencyGraph;
using dependency::nodes_T;

TEST(DirectoryCollapserTest, collapse) {
    DependencyGraph graph;
    graph.add("/a/b/c/ab/A.cpp", { "/a/b/c/ab/A.h", "/a/b/c/ab/B.h" });
    graph.add("/a/b/c/ab/A.h", { "/a/b/c/cd/C.h", "/a/b/c/cd/D.h" });
    graph.add("/a/b/c/ab/B.cpp", { "/a/b/c/ab/B.h", "/a/b/c/cd/C.h" });
    graph.add("/a/b/c/ab/B.h", { "/a/b/c/cd/C.h" });
    graph.add("/a/b/c/cd/C.cpp", { "/a/b/c/cd/C.h" });

    const nodes_T expected_nodes = { "/a/b/c/ab", "/a/b/c/cd" };
    const nodes_T deps_AB = { "/a/b/c/cd" };
    const nodes_T deps_CD = {};
    const DirectoryCollapser collapser;
    const Changer& changer = collapser;

    changer.change(graph);
    EXPECT_EQ(graph.nodes().size(), 2u);
    auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);
    EXPECT_EQ(graph.dependencies("/a/b/c/ab"), deps_AB);
    EXPECT_EQ(graph.dependencies("/a/b/c/cd"), deps_CD);
}
}
