#include <controller/Highlight.h>
#include <util/Util.h>

#include <algorithm>
#include <stdexcept>

// check https://designsystem.digital.gov/design-tokens/color/state-tokens/

const std::vector<dependency::Color> dependency::AUTO_COLORS = {
    { "#009ec1", "#e7f6f8" }, // blue 1
    { "#b51d09", "#f4e3db" }, // red 1
    { "#936f38", "#faf3d1" }, // yellow 1
    { "#4d8055", "#ecf3ec" }, // green 1
    { "#2e6276", "#9ddfeb" }, // blue 2
    { "#6f3331", "#f39268" }, // red 2
    { "#936f38", "#fee685" }, // yellow 2
    { "#446443", "#70e17b" }, // green 2
    { "#e7f6f8", "#009ec1" }, // blue 3
    { "#f4e3db", "#b51d09" }, // red 3
    { "#faf3d1", "#e5a000" }, // yellow 3
    { "#ecf3ec", "#4d8055" }, // green 3
    { "#9ddfeb", "#2e6276" }, // blue 4
    { "#f39268", "#6f3331" }, // red 4
    { "#fee685", "#936f38" }, // yellow 4
    { "#70e17b", "#446443" }, // green 4
    { "#000000", "#00bde3" }, // blue 5
    { "#ffffff", "#d63e04" }, // red 5
    { "#000000", "#ffbe2e" }, // yellow 5
    { "#ffffff", "#00a91c" }, // green 5
};

namespace dependency {

Highlight Highlight::create(const std::string& highlight) {
    const std::vector<std::string> tokens = util::split(highlight, ',');

    switch (tokens.size()) {
        case 3:
            return { tokens[0], { tokens[1], tokens[2] } };
        case 1:
            return { tokens[0], { "blue", "lightcyan" } };
        default:
            throw std::invalid_argument("Highlight - invalid specification [" + highlight + "]");
    }
}

std::vector<Highlight> Highlight::create(const std::vector<std::string>& highlights) {
    std::vector<Highlight> res;
    res.reserve(highlights.size());
    std::transform(highlights.begin(), highlights.end(), std::back_inserter(res), [](const std::string& highlight) {
        return create(highlight);
    });
    return res;
}
}
