#include <reader/GccReader.h>
#include <reader/GccReaderHelper.h>
#include <util/Util.h>

namespace reader {

using dependency::DependencyGraph;
using dependency::node_T;
using dependency::nodes_T;

GccReader::GccReader(std::string project_path,
      node_T full_path,
      std::string gcc_options)
      : GccReader(std::move(project_path), std::set<std::string>{ std::move(full_path) }, std::move(gcc_options)) {
}

GccReader::GccReader(std::string project_path,
      nodes_T full_paths,
      std::string gcc_options)
      : project_path_(std::move(project_path)),
        full_paths_(std::move(full_paths)),
        gcc_options_(std::move(gcc_options)) {
}

DependencyGraph GccReader::read() const {
    DependencyGraph graph;
    nodes_T to_process(full_paths_.begin(), full_paths_.end());
    nodes_T processed;
    while (not to_process.empty()) {
        const auto node = to_process.begin();
        if (not util::is_valid_file(*node)) {
            to_process.erase(*node);
            continue;
        }
        const auto node_deps = impl::process_file(project_path_, *node, gcc_options_);
        graph.add(*node, node_deps);

        processed.insert(*node);
        to_process.erase(node);
        for (const auto& dependency : node_deps) {
            if (processed.find(dependency) == processed.end()) {
                to_process.insert(dependency);
            }
        }
    }
    return graph;
}
}
