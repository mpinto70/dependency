#pragma once

#include <controller/Changer.h>

#include <string>
#include <vector>

namespace changer {

class Remover : public dependency::Changer {
public:
    explicit Remover(const std::vector<std::string>& masks);
    ~Remover() override = default;
    void change(dependency::DependencyGraph& graph) const override;

private:
    std::vector<std::string> masks_;
};
}
