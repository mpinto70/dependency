#pragma once

#include <controller/Highlight.h>
#include <controller/Writer.h>

namespace writer {

class Dotter : public dependency::Writer {
public:
    void write(std::ostream& out,
          const dependency::DependencyGraph& graph,
          const std::vector<dependency::Highlight>& highlights) const override;
};
}
