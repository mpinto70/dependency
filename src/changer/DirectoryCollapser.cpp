#include <changer/DirectoryCollapser.h>

#include <filesystem>

namespace changer {

using dependency::DependencyGraph;
using dependency::node_T;
using dependency::nodes_T;
namespace fs = std::filesystem;

namespace {
std::string leaf_directory(const std::string& file_name) {
    const fs::path path(file_name);
    const auto dir = path.parent_path().string();
    if (dir.empty()) {
        return "/";
    } else {
        return dir;
    }
}
}

void DirectoryCollapser::change(DependencyGraph& graph) const {
    DependencyGraph res;
    for (const auto& node : graph.nodes()) {
        nodes_T dependencies;
        const auto stripped_node = leaf_directory(node);
        for (const auto& deps : graph.dependencies(node)) {
            const auto stripped_dep = leaf_directory(deps);
            if (stripped_dep != stripped_node) {
                dependencies.insert(stripped_dep);
            }
        }
        res.add(stripped_node, dependencies);
    }

    graph.swap(res);
}
}
