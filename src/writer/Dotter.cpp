#include <writer/Dotter.h>

#include <algorithm>
#include <iostream>
#include <set>

namespace writer {
using dependency::DependencyGraph;

namespace {

std::string file_to_node(std::string file_name) {
    std::replace_if(
          file_name.begin(),
          file_name.end(),
          [](char c) {
              return c == '/' || c == '.' || c == '-' || c == '+';
          },
          '_');

    return file_name;
}

std::string file_to_name(std::string file_name) {
    const auto pos = file_name.find_last_of('/');
    if (pos == std::string::npos)
        return file_name;
    file_name.replace(pos, 1, "\\n");

    return file_name;
}
}

void Dotter::write(std::ostream& out,
      const DependencyGraph& graph,
      const std::vector<dependency::Highlight>& highlights) const {
    out << "digraph g\n{\n";
    out << "    rankdir=LR;\n";
    out << "    node [ shape=record, fontname=Helvetica ];\n";
    out << "\n    // ========Nodes========\n";

    std::set<std::string> file_paths;
    for (const auto& node : graph.nodes()) {
        file_paths.insert(node);
    }

    for (const auto& file_path : file_paths) {
        const auto dot_node = file_to_node(file_path);
        const auto dot_name = file_to_name(file_path);

        auto it = std::find_if(highlights.begin(), highlights.end(), [&file_path](const dependency::Highlight& highlight) {
            return file_path.find(highlight.text) != std::string::npos;
        });
        if (it != highlights.end()) {
            out << "    " << dot_node << " [ label=\"" << dot_name << "\" ";
            out << "fontcolor=\"" << it->color.foreground << "\", color=\"" << it->color.foreground << '"';
            out << ", style=filled, fillcolor=\"" << it->color.background << "\" ];\n";
        } else {
            out << "    " << dot_node << " [ label=\"" << dot_name << "\" ];\n";
        }
    }

    out << "\n    // ========Dependencies========\n";
    for (const auto& node : graph.nodes()) {
        const auto dot_node = file_to_node(node);
        const auto dependencies = graph.dependencies(node);
        if (dependencies.empty())
            continue;
        for (const auto& dependency : dependencies) {
            const auto dep = file_to_node(dependency);
            out << "    " << dot_node << " -> " << dep << ";\n";
        }
        out << "\n";
    }
    out << "}\n";
}
}
