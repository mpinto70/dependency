#pragma once

#include <controller/DependencyGraph.h>

namespace dependency {

class Changer {
public:
    virtual ~Changer() = default;

    virtual void change(DependencyGraph& graph) const = 0;
};
}
