
add_library(
    preprocessor
    Preprocessor.cpp
    PathResolver.cpp
)

add_dependencies(preprocessor brasa_argparse)
