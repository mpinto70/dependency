#pragma once

#include <string>
#include <vector>

namespace preprocessor {
class PathResolver {
public:
    static std::string canonicalize(const std::string& path);
    static std::vector<std::string> resolve_dir(const std::string& path);
    static std::vector<std::string> resolve(const std::vector<std::string>& paths);
};
}
