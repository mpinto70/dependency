#include <preprocessor/Preprocessor.h>

#include <brasa/argparse/ArgParser.h>

#include <iostream>

namespace preprocessor {

namespace {
void process_parameters(int argc,
      char* const argv[],
      std::string& project_path,
      options_t& files,
      options_t& ignores,
      options_t& cuts,
      options_t& options,
      options_t& highlights,
      AutoHighlight& auto_highlight,
      CollapseType& collapse_type) {
    using brasa::argparse::BooleanParser;
    using brasa::argparse::make_parser;
    using brasa::argparse::MultiValue;
    using brasa::argparse::ParseResult;
    using brasa::argparse::SingleValue;
    using brasa::argparse::ValueParser;
    const std::string description = "Program that build a dependency graph for C++ projects";
    const std::string footer = "Highlight and Auto Highlight\n"
                               "  Highlight option (-H) accepts either\n"
                               "    * <text> in witch case it will highlight in blue over lightcyan\n"
                               "    * <text,foreground,background> in which case it will highlight in foreground over background\n"
                               "    highlights are applied in the order of their definition in the command line\n"
                               "  Auto highlight option (-A) makes the highlight apply to each distinct directory for the nodes\n"
                               "    that have no other highlight specified by a -H option\n"
                               "  Please, check https://www.graphviz.org/doc/info/colors.html for the possible colors\n";

    auto parser = make_parser(description,
          std::make_tuple(
                SingleValue<std::string>("PROJECT_ROOT", "path to root of project (to avoid including files outside of project"),
                MultiValue<std::string>("FILE|DIR...", "name of files or directories to process")),
          std::make_tuple(
                ValueParser<MultiValue<std::string>>('i', "ignore", "text", "text that will be used to ignore files that match it"),
                ValueParser<MultiValue<std::string>>('c', "cut", "text", "text that will be removed from file path"),
                ValueParser<MultiValue<std::string>>('o', "option", "text", "compiler option (forwarded)"),
                ValueParser<MultiValue<std::string>>('H', "highlight", "text", "text in cell that cause it to be highlighted (see below)"),
                BooleanParser('A', "auto_highlight", "if the system should auto highlight (see below)"),
                BooleanParser('s', "collapse_file", "if .h and .cpp should be treated as components"),
                BooleanParser('d', "collapse_dir", "if directories should be treated as nodes")),
          footer);

    switch (parser.parse(argc, argv, std::cerr)) {
        case ParseResult::Ok: {
            project_path = std::get<0>(parser.values()).value();
            files = std::get<1>(parser.values()).values();
            ignores = std::get<0>(parser.parsers()).digester().values();
            cuts = std::get<1>(parser.parsers()).digester().values();
            options = std::get<2>(parser.parsers()).digester().values();
            highlights = std::get<3>(parser.parsers()).digester().values();
            auto_highlight = std::get<4>(parser.parsers()).value() ? AutoHighlight::Yes : AutoHighlight::No;
            const bool collapse_file = std::get<5>(parser.parsers()).value();
            const bool collapse_dir = std::get<6>(parser.parsers()).value();
            if (collapse_dir) {
                collapse_type = CollapseType::Directory;
            } else if (collapse_file) {
                collapse_type = CollapseType::File;
            } else {
                collapse_type = CollapseType::None;
            }
            break;
        }
        case ParseResult::Error:
            std::cerr << "\n"
                      << parser.usage(argv[0]) << "\n";
            exit(1);

        case ParseResult::Help:
            exit(0);
    }
}
}

Preprocessor::Preprocessor(std::string project_path,
      options_t files,
      options_t ignores,
      options_t cuts,
      options_t options,
      options_t highlights,
      AutoHighlight auto_highlight,
      CollapseType collapse_type)
      : project_path_(std::move(project_path)),
        files_(std::move(files)),
        ignores_(std::move(ignores)),
        cuts_(std::move(cuts)),
        options_(std::move(options)),
        highlights_(std::move(highlights)),
        auto_highlight_(auto_highlight),
        collapse_type_(collapse_type) {
}

Preprocessor Preprocessor::create(int argc, char* const argv[]) {
    std::string project_path;
    options_t files;
    options_t ignores;
    options_t cuts;
    options_t options;
    options_t highlights;
    auto collapse_type = CollapseType::None;
    auto auto_highlight = AutoHighlight::No;
    process_parameters(argc, argv, project_path, files, ignores, cuts, options, highlights, auto_highlight, collapse_type);
    return Preprocessor(project_path, files, ignores, cuts, options, highlights, auto_highlight, collapse_type);
}
}
