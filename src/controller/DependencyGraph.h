#pragma once

#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace dependency {
using node_T = std::string;
using nodes_T = std::set<node_T>;

class DependencyGraph {
public:
    nodes_T nodes() const;

    nodes_T dependencies(const node_T& node_name) const;

    void add(const node_T& node_name, const nodes_T& dependencies = {});

    template <typename F>
    void change(F f) {
        nodes_nodes_T new_deps;
        for (const auto& it : dependencies_) {
            const node_T node = f(it.first);
            nodes_T node_deps;
            std::transform(it.second.begin(), it.second.end(), std::inserter(node_deps, node_deps.begin()), [&f](const auto& node) {
                return f(node);
            });
            new_deps.emplace(node, node_deps);
        }
        dependencies_.swap(new_deps);
    }

    void remove(const node_T& node_name);

    bool exist(const node_T& node_name) const;

    template <typename F>
    void remove_if(F f) {
        std::set<node_T> removed;
        for (auto it = dependencies_.begin(); it != dependencies_.end(); /* will rease*/) {
            if (f(it->first)) {
                removed.insert(it->first);
                it = dependencies_.erase(it);
            } else {
                ++it;
            }
        }

        std::for_each(dependencies_.begin(), dependencies_.end(), [&removed](auto& pair) {
            for (const auto& node : removed) {
                pair.second.erase(node);
            }
        });
    }

    void swap(DependencyGraph& x) {
        dependencies_.swap(x.dependencies_);
    }

    friend bool operator==(const DependencyGraph& x, const DependencyGraph& y) {
        return x.dependencies_ == y.dependencies_;
    }

private:
    using nodes_nodes_T = std::map<node_T, nodes_T>;

    nodes_nodes_T dependencies_;
};
}
