#include <util/Util.h>

#include <algorithm>
#include <filesystem>
#include <set>

namespace util {
bool is_valid_file(const std::string& file) {
    const std::set<std::string> EXTENSIONS = { ".h", ".H", ".hpp", ".hh", ".hxx", ".c", ".C", ".cpp", ".cc", ".cxx" };
    const std::string extension = std::filesystem::path(file).extension();
    return not extension.empty() && EXTENSIONS.find(extension) != EXTENSIONS.end();
}

std::vector<std::string> split(const std::string& str, char sep) {
    std::vector<std::string> tokens;
    size_t pos = 0, last_pos = 0;
    while ((pos = str.find(sep, pos)) != std::string::npos) {
        tokens.push_back(str.substr(last_pos, pos - last_pos));
        ++pos;
        last_pos = pos;
    }
    tokens.push_back(str.substr(last_pos));

    return tokens;
}

std::string trim(std::string str, char blank) {
    const auto last_pos = str.find_last_not_of(blank);
    if (last_pos == std::string::npos)
        return "";

    str.erase(last_pos + 1);

    const auto first_pos = str.find_first_not_of(blank);
    str.erase(0, first_pos);

    return str;
}

bool starts_with(const std::string& str, const std::string& start) {
    return start.size() <= str.size() && str.compare(0, start.size(), start) == 0;
}

std::string join(const std::vector<std::string>& tokens, char joiner) {
    std::string out;
    std::for_each(tokens.begin(), tokens.end(), [&out, joiner](const std::string& s) { out += s + joiner; });
    if (not out.empty())
        out.pop_back();
    return out;
}

}
