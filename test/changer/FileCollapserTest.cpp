#include <changer/FileCollapser.h>

#include <gtest/gtest.h>

namespace changer {
using dependency::Changer;
using dependency::DependencyGraph;
using dependency::nodes_T;

TEST(FileCollapserTest, no_collapse) {
    DependencyGraph graph;
    graph.add("A", { "B" });
    graph.add("C", { "D" });
    graph.add("E", { "F" });

    const FileCollapser collapser;
    const Changer& changer = collapser;

    const DependencyGraph expected = graph;
    changer.change(graph);
    EXPECT_EQ(graph, expected);
}

void print(const std::string& msg, const DependencyGraph& graph) {
    std::cerr << "----- " << msg << " -----" << std::endl;
    for (const auto& node : graph.nodes()) {
        std::cerr << node << ": ";
        for (const auto& dep : graph.dependencies(node)) {
            std::cerr << dep << " | ";
        }
        std::cerr << std::endl;
    }
}

TEST(FileCollapserTest, collapse) {
    DependencyGraph graph;
    graph.add("A.cpp", { "A.h", "B.h" });
    graph.add("A.h", { "C.h", "D.h" });
    graph.add("B.cpp", { "B.h", "C.h" });
    graph.add("B.h", { "C.h" });
    graph.add("C.cpp", { "C.h" });

    const nodes_T expected_nodes = { "A", "B", "C", "D" };
    const nodes_T deps_A = { "B", "C", "D" };
    const nodes_T deps_B = { "C" };
    const nodes_T deps_C = {};
    const nodes_T deps_D = {};
    const FileCollapser collapser;
    const Changer& changer = collapser;

    changer.change(graph);
    EXPECT_EQ(graph.nodes().size(), 4u);
    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);
    EXPECT_EQ(graph.dependencies("A"), deps_A);
    EXPECT_EQ(graph.dependencies("B"), deps_B);
    EXPECT_EQ(graph.dependencies("C"), deps_C);
    EXPECT_EQ(graph.dependencies("D"), deps_D);
}
}
