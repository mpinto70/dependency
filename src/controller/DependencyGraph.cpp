#include <controller/DependencyGraph.h>

namespace dependency {
void DependencyGraph::add(const node_T& node_name, const nodes_T& deps) {
    dependencies_[node_name].insert(deps.begin(), deps.end());
    for (const auto& node : deps) {
        dependencies_[node];
    }
}

nodes_T DependencyGraph::nodes() const {
    nodes_T res;
    std::transform(dependencies_.begin(), dependencies_.end(), std::inserter(res, res.begin()), [](const auto& pair) { return pair.first; });
    return res;
}

nodes_T DependencyGraph::dependencies(const node_T& node_name) const {
    const auto it = dependencies_.find(node_name);
    return (it != dependencies_.end()) ? it->second : nodes_T();
}

void DependencyGraph::remove(const node_T& node_name) {
    dependencies_.erase(node_name);
    for (auto& it : dependencies_) {
        it.second.erase(node_name);
    }
}

bool DependencyGraph::exist(const node_T& node_name) const {
    return dependencies_.find(node_name) != dependencies_.end();
}
}
