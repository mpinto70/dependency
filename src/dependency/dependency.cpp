#include <changer/Cutter.h>
#include <changer/DirectoryCollapser.h>
#include <changer/FileCollapser.h>
#include <changer/Remover.h>
#include <controller/Controller.h>
#include <preprocessor/PathResolver.h>
#include <preprocessor/Preprocessor.h>
#include <reader/GccReader.h>
#include <util/Util.h>
#include <writer/Dotter.h>

#include <iostream>

namespace {
std::string gcc_options(const preprocessor::options_t& options) {
    return util::join(options, ' ');
}
}

int main(int argc, char* argv[]) {
    using changer::Cutter;
    using changer::DirectoryCollapser;
    using changer::FileCollapser;
    using changer::Remover;
    using dependency::Changer;
    using dependency::Controller;
    using dependency::Highlight;
    using dependency::Reader;
    using dependency::Writer;
    using preprocessor::AutoHighlight;
    using preprocessor::CollapseType;
    using preprocessor::PathResolver;
    using preprocessor::Preprocessor;
    using reader::GccReader;
    using writer::Dotter;

    try {
        const auto preprocessor = Preprocessor::create(argc, argv);
        const auto project_path = PathResolver::canonicalize(preprocessor.project_path());
        const auto files = PathResolver::resolve(preprocessor.files());
        const dependency::nodes_T nodes(files.begin(), files.end());
        const auto options = gcc_options(preprocessor.options());
        const std::unique_ptr<Reader> reader = std::make_unique<GccReader>(project_path, nodes, options);
        std::vector<std::unique_ptr<Changer>> changers;
        switch (preprocessor.collapse_type()) {
            case CollapseType::File:
                changers.push_back(std::make_unique<FileCollapser>());
                break;
            case CollapseType::Directory:
                changers.push_back(std::make_unique<DirectoryCollapser>());
                break;
            case CollapseType::None:
                break;
        }
        changers.push_back(std::make_unique<Remover>(preprocessor.ignores()));
        changers.push_back(std::make_unique<Cutter>(preprocessor.cuts()));
        const std::unique_ptr<Writer> writer = std::make_unique<Dotter>();

        const bool auto_highlight = preprocessor.auto_highlight() == AutoHighlight::Yes;
        const auto highlights = dependency::Highlight::create(preprocessor.highlights());
        Controller::run(reader, changers, writer, highlights, auto_highlight, std::cout);
    } catch (const std::exception& e) {
        std::cerr << "an error occurred" << std::endl;
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
