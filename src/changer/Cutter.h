#pragma once

#include <controller/Changer.h>

#include <string>
#include <vector>

namespace changer {

class Cutter : public dependency::Changer {
public:
    explicit Cutter(std::vector<std::string> cuts);
    ~Cutter() override = default;
    void change(dependency::DependencyGraph& graph) const override;

private:
    std::vector<std::string> cuts_;
    size_t sizeof_cut(const std::string& name) const;
};
}
