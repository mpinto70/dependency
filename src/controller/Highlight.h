#pragma once

#include <string>
#include <vector>

namespace dependency {
struct Color {
    std::string foreground;
    std::string background;
};
extern const std::vector<Color> AUTO_COLORS;

struct Highlight {
    std::string text;
    Color color;

    static Highlight create(const std::string& highlight);
    static std::vector<Highlight> create(const std::vector<std::string>& highlights);
};
}
