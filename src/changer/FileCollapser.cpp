#include <changer/FileCollapser.h>

#include <filesystem>

namespace changer {

using dependency::DependencyGraph;
using dependency::node_T;
using dependency::nodes_T;
namespace fs = std::filesystem;

namespace {
std::string file_no_extension(const std::string& file_name) {
    const std::string extension = fs::path(file_name).extension();
    if (extension.empty()) {
        return file_name;
    } else {
        return file_name.substr(0, file_name.size() - extension.size());
    }
}
}

void FileCollapser::change(DependencyGraph& graph) const {
    DependencyGraph res;
    for (const auto& node : graph.nodes()) {
        nodes_T dependencies;
        const auto stripped_node = file_no_extension(node);
        for (const auto& deps : graph.dependencies(node)) {
            const auto stripped_dep = file_no_extension(deps);
            if (stripped_dep != stripped_node)
                dependencies.insert(stripped_dep);
        }
        res.add(stripped_node, dependencies);
    }

    graph.swap(res);
}
}
