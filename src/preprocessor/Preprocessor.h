#pragma once

#include <ostream>
#include <set>
#include <string>
#include <vector>

namespace preprocessor {

using options_t = std::vector<std::string>;

enum class CollapseType {
    None,
    File,
    Directory,
};

enum class AutoHighlight {
    No,
    Yes,
};

/** Breaks command line in its components. */
class Preprocessor {
public:
    Preprocessor(std::string project_path,
          options_t files,
          options_t ignores,
          options_t cuts,
          options_t options,
          options_t highlights,
          AutoHighlight auto_highlight,
          CollapseType collapse_type);
    ~Preprocessor() = default;

    const std::string& project_path() const { return project_path_; }
    const options_t& files() const { return files_; }
    const options_t& ignores() const { return ignores_; }
    const options_t& cuts() const { return cuts_; }
    const options_t& options() const { return options_; }
    const options_t& highlights() const { return highlights_; }
    AutoHighlight auto_highlight() const { return auto_highlight_; }
    CollapseType collapse_type() const { return collapse_type_; }

    static Preprocessor create(int argc, char* const argv[]);

private:
    std::string project_path_;
    options_t files_;
    options_t ignores_;
    options_t cuts_;
    options_t options_;
    options_t highlights_;
    AutoHighlight auto_highlight_;
    CollapseType collapse_type_;
};
}
