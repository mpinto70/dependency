#include <changer/Cutter.h>

#include <util/Util.h>

#include <algorithm>

namespace changer {
using dependency::DependencyGraph;

Cutter::Cutter(std::vector<std::string> cuts)
      : cuts_(std::move(cuts)) {
}

void Cutter::change(DependencyGraph& graph) const {
    using dependency::node_T;
    graph.change([&](const node_T& node) { return node.substr(sizeof_cut(node)); });
}

size_t Cutter::sizeof_cut(const std::string& name) const {
    const auto cut_it = std::find_if(cuts_.begin(), cuts_.end(), [&name](const std::string& cut) {
        return util::starts_with(name, cut) && name != cut;
    });
    if (cut_it != cuts_.end())
        return cut_it->size();
    else
        return 0;
}
}
