#include <changer/Remover.h>

#include <gtest/gtest.h>

namespace changer {
using dependency::Changer;
using dependency::DependencyGraph;

// A-->B
// C-->D
// E-->F
TEST(RemoverTest, no_remove) {
    DependencyGraph graph;
    graph.add("A", { "B" });
    graph.add("C", { "D" });
    graph.add("E", { "F" });

    const Remover remover({ "XYX", "ABC" });
    const Changer& changer = remover;

    const DependencyGraph expected = graph;
    changer.change(graph);
    EXPECT_EQ(graph, expected);
}

TEST(RemoverTest, remove) {
    DependencyGraph graph;
    graph.add("AB", { "EF", "GH" });
    graph.add("XABX", { "EF", "GH" });
    graph.add("CD", { "EF", "GH" });
    graph.add("EF", { "AB", "CD", "GH" });
    graph.add("GH", { "EF", "AB", "CD" });
    graph.add("IJ", { "KL" });
    graph.add("ab", { "cd" });
    graph.add("cd", { "ab" });
    DependencyGraph expected;
    expected.add("EF", { "GH" });
    expected.add("GH", { "EF" });
    expected.add("IJ", { "KL" });
    expected.add("ab", { "cd" });
    expected.add("cd", { "ab" });

    const Remover remover({ "AB", "CD" });
    const Changer& changer = remover;

    changer.change(graph);
    EXPECT_EQ(graph, expected);
}
}
