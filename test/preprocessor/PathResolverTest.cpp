#include <preprocessor/PathResolver.h>
#include <util/Util.h>

#include <gtest/gtest.h>

#include <filesystem>
#include <sstream>
#include <string>

namespace preprocessor {
namespace fs = std::filesystem;

namespace {
const std::string FILES_ROOT = PROJECT_DIR "/test/preprocessor";
const std::string TEST_FILES_ROOT = PROJECT_DIR "/test/files";

std::string relative_path_to(const std::string& absolute, const std::string& root) {
    std::vector<std::string> curdir_broken = util::split(root, '/');
    std::vector<std::string> relative_broken = util::split(absolute, '/');

    while (not curdir_broken.empty()
           && not relative_broken.empty()
           && curdir_broken[0] == relative_broken[0]) {
        curdir_broken.erase(curdir_broken.begin());
        relative_broken.erase(relative_broken.begin());
    }

    std::string relative = "./";
    for (size_t i = 0; i < curdir_broken.size(); ++i) {
        relative += "../";
    }

    relative += util::join(relative_broken, '/');
    return relative;
}
}

TEST(PathResolverTest, canonicalize_inexistent_throws) {
    EXPECT_THROW(PathResolver::canonicalize("nonexistent.cpp"), std::runtime_error);
}

TEST(PathResolverTest, canonicalize_absolute_returns_absolute) {
    const std::string file = FILES_ROOT + "/PathResolverTest.cpp";
    EXPECT_EQ(PathResolver::canonicalize(file), file);
}

TEST(PathResolverTest, canonicalize_relative_returns_absolute) {
    const auto rundir = fs::canonical(fs::path("./")).string();

    const std::string file = FILES_ROOT + "/PathResolverTest.cpp";
    const auto relative = relative_path_to(file, rundir);
    EXPECT_NE(relative, file);
    EXPECT_EQ(PathResolver::canonicalize(relative), file);
}

TEST(PathResolverTest, resolve_dir) {
    const std::vector<std::string> expected = {
        FILES_ROOT + "/PathResolverTest.cpp",
        FILES_ROOT + "/PreprocessorTest.cpp",
    };

    auto files = PathResolver::resolve_dir(FILES_ROOT);
    std::sort(files.begin(), files.end());

    EXPECT_EQ(files, expected);
}

TEST(PathResolverTest, resolve_dir_recursive) {
    const std::vector<std::string> expected = {
        TEST_FILES_ROOT + "/src/dir1/file1_1.cpp",
        TEST_FILES_ROOT + "/src/dir1/file1_1.h",
        TEST_FILES_ROOT + "/src/dir1/file1_2.cpp",
        TEST_FILES_ROOT + "/src/dir1/file1_2.h",
        TEST_FILES_ROOT + "/src/dir1/file1_3.cpp",
        TEST_FILES_ROOT + "/src/dir2/file2_1.cpp",
        TEST_FILES_ROOT + "/src/dir2/file2_1.h",
        TEST_FILES_ROOT + "/src/dir2/file2_2.cpp",
        TEST_FILES_ROOT + "/src/dir2/file2_2.h",
        TEST_FILES_ROOT + "/src/dir2/relative.cpp",
    };

    auto files = PathResolver::resolve_dir(TEST_FILES_ROOT);
    std::sort(files.begin(), files.end());

    EXPECT_EQ(files, expected);
}

TEST(PathResolverTest, multiple_files) {
    const std::vector<std::string> files = {
        PROJECT_DIR "/test/preprocessor/PathResolverTest.cpp",
        PROJECT_DIR "/src/preprocessor/PathResolver.cpp",
    };

    EXPECT_EQ(PathResolver::resolve(files), files);
}

TEST(PathResolverTest, multiple_files_relative) {
    const auto rundir = fs::canonical(fs::path("./")).string();
    const std::vector<std::string> relatives = {
        relative_path_to(PROJECT_DIR "/test/preprocessor/PathResolverTest.cpp", rundir),
        relative_path_to(PROJECT_DIR "/src/preprocessor/PathResolver.cpp", rundir),
    };
    const std::vector<std::string> expected = {
        PROJECT_DIR "/test/preprocessor/PathResolverTest.cpp",
        PROJECT_DIR "/src/preprocessor/PathResolver.cpp",
    };

    EXPECT_EQ(PathResolver::resolve(relatives), expected);
}

TEST(PathResolverTest, multiple_files_and_dir) {
    const std::vector<std::string> expected = {
        TEST_FILES_ROOT + "/src/dir1/file1_1.cpp",
        TEST_FILES_ROOT + "/src/dir1/file1_1.h",
        TEST_FILES_ROOT + "/src/dir1/file1_2.cpp",
        TEST_FILES_ROOT + "/src/dir1/file1_2.h",
        TEST_FILES_ROOT + "/src/dir1/file1_3.cpp",
        TEST_FILES_ROOT + "/src/dir2/file2_1.cpp",
        TEST_FILES_ROOT + "/src/dir2/file2_1.h",
        TEST_FILES_ROOT + "/src/dir2/file2_2.cpp",
        TEST_FILES_ROOT + "/src/dir2/file2_2.h",
        TEST_FILES_ROOT + "/src/dir2/relative.cpp",
        FILES_ROOT + "/PathResolverTest.cpp",
    };

    const std::vector<std::string> source = {
        TEST_FILES_ROOT,                                       // dir
        PROJECT_DIR "/test/preprocessor/PathResolverTest.cpp", // file
    };
    auto files = PathResolver::resolve(source);
    std::sort(files.begin(), files.end());

    EXPECT_EQ(files, expected);
}
}
