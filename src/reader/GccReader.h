#pragma once

#include <controller/Reader.h>

namespace reader {
/** Generates a dependency graph based on gcc command and options. */
class GccReader : public dependency::Reader {
public:
    GccReader(std::string project_path,
          dependency::node_T full_path,
          std::string gcc_options);

    GccReader(std::string project_path,
          dependency::nodes_T full_paths,
          std::string gcc_options);

    dependency::DependencyGraph read() const override;

private:
    std::string project_path_;
    dependency::nodes_T full_paths_;
    std::string gcc_options_;
};
}
