#include <util/Util.h>

#include <gtest/gtest.h>

namespace util {
TEST(UtilTest, valid_c_file) {
    EXPECT_TRUE(is_valid_file("file.cpp"));
    EXPECT_TRUE(is_valid_file("file.cxx"));
    EXPECT_TRUE(is_valid_file("file.C"));
    EXPECT_TRUE(is_valid_file("file.cc"));
    EXPECT_TRUE(is_valid_file("file.c"));
    EXPECT_TRUE(is_valid_file("file.hpp"));
    EXPECT_TRUE(is_valid_file("file.hxx"));
    EXPECT_TRUE(is_valid_file("file.H"));
    EXPECT_TRUE(is_valid_file("file.hh"));
    EXPECT_TRUE(is_valid_file("file.h"));

    EXPECT_FALSE(is_valid_file("file.inl"));
    EXPECT_FALSE(is_valid_file("file.txt"));
    EXPECT_FALSE(is_valid_file("Makefile"));
}

namespace {
void check_split(const std::string& str, char sep, const std::vector<std::string>& tokens) {
    EXPECT_EQ(split(str, sep), tokens);
}
}

TEST(UtilTest, split) {
    check_split("/a/b/c", '/', { "", "a", "b", "c" });
    check_split("//b/c/", '/', { "", "", "b", "c", "" });
    check_split("some words here", ' ', { "some", "words", "here" });
}

TEST(UtilTest, trim) {
    EXPECT_EQ(trim("   some string  "), "some string");
    EXPECT_EQ(trim("   some string  ", ' '), "some string");
    EXPECT_EQ(trim("///some/string//", '/'), "some/string");
}

TEST(UtilTest, starts_with) {
    EXPECT_TRUE(starts_with("   some string  ", "   "));
    EXPECT_TRUE(starts_with("whole string", "whole string"));
    EXPECT_FALSE(starts_with("whole string", "Whole"));
    EXPECT_FALSE(starts_with("whole string", "wholE"));
    EXPECT_FALSE(starts_with("whole string", "whole string and more"));
}

namespace {
void check_join(const std::vector<std::string>& tokens, char joiner, const std::string& expected_result) {
    EXPECT_EQ(join(tokens, joiner), expected_result);
}
}

TEST(UtilTest, join) {
    check_join({ "a", "b", "cde", "fghi" }, ',', "a,b,cde,fghi");
    check_join({ "a", "", "cde", "fghi" }, ',', "a,,cde,fghi");
}

namespace {
void check_split_join(const std::vector<std::string>& tokens, char joiner, const std::string& joined) {
    EXPECT_EQ(join(tokens, joiner), joined);
    EXPECT_EQ(split(joined, joiner), tokens);
}
}

TEST(UtilTest, split_and_join) {
    check_split_join({ "a", "b", "cde", "fghi" }, ',', "a,b,cde,fghi");
    check_split_join({ "a", "", "cde", "fghi" }, ',', "a,,cde,fghi");
}
}
