#include <reader/GccReader.h>
#include <reader/GccReaderHelper.h>

#include <gtest/gtest.h>

namespace reader {
namespace {
using dependency::node_T;
using dependency::nodes_T;

const std::string FILES_ROOT = PROJECT_DIR "/test/files/src";

const nodes_T file1_1_cpp_deps = {
    FILES_ROOT + "/dir1/file1_1.h",
};

const nodes_T file1_1_h_deps = {
    FILES_ROOT + "/dir1/file1_2.h",
};

const nodes_T file1_2_h_deps = {
    FILES_ROOT + "/dir2/file2_1.h",
};

const nodes_T file2_1_h_deps = {
    FILES_ROOT + "/dir2/file2_2.h",
};

const nodes_T relative_cpp_deps = {
    FILES_ROOT + "/dir2/file2_2.h",
};

// the redundant dependency from file1_3.cpp -> file1_2.h is due to
// the fact that the dependency is inverted in file1_3.cpp
const nodes_T file1_3_cpp_deps = {
    FILES_ROOT + "/dir1/file1_1.h",
    FILES_ROOT + "/dir1/file1_2.h",
};

// the dependency file1_2.cpp --> file2_1.h does not appear because it
// is removed by file1_2.h (#pragma once). So it is a second level only
// for file1_2.cpp
const nodes_T file1_2_cpp_deps = {
    FILES_ROOT + "/dir1/file1_2.h",
};

const nodes_T file2_1_cpp_deps = {
    FILES_ROOT + "/dir2/file2_1.h",
    FILES_ROOT + "/dir2/templates.inl",
};

const nodes_T file2_2_cpp_deps = {
    FILES_ROOT + "/dir2/file2_2.h",
};

const nodes_T file2_2_h_deps = {};
}

TEST(GccReaderTest, read_one_file_1) {
    const GccReader reader(FILES_ROOT, FILES_ROOT + "/dir2/file2_2.cpp", "-std=c++14 -I " + FILES_ROOT);
    const auto graph = reader.read();
    const nodes_T expected_nodes = {
        FILES_ROOT + "/dir2/file2_2.cpp",
        FILES_ROOT + "/dir2/file2_2.h",
    };

    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);

    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_2.cpp"), file2_2_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_2.h"), file2_2_h_deps);
}

TEST(GccReaderTest, read_one_file_2) {
    const GccReader reader(FILES_ROOT, FILES_ROOT + "/dir1/file1_1.cpp", "-std=c++14 -I " + FILES_ROOT);
    const auto graph = reader.read();
    const nodes_T expected_nodes = {
        FILES_ROOT + "/dir1/file1_1.cpp",
        FILES_ROOT + "/dir1/file1_1.h",
        FILES_ROOT + "/dir1/file1_2.h",
        FILES_ROOT + "/dir2/file2_1.h",
        FILES_ROOT + "/dir2/file2_2.h",
    };

    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);

    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_1.cpp"), file1_1_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_1.h"), file1_1_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_2.h"), file1_2_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_1.h"), file2_1_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_2.h"), file2_2_h_deps);
}

TEST(GccReaderTest, read_one_file_3) {
    const GccReader reader(FILES_ROOT, FILES_ROOT + "/dir1/file1_2.cpp", "-std=c++14 -I " + FILES_ROOT);
    const auto graph = reader.read();
    const nodes_T expected_nodes = {
        FILES_ROOT + "/dir1/file1_2.cpp",
        FILES_ROOT + "/dir1/file1_2.h",
        FILES_ROOT + "/dir2/file2_1.h",
        FILES_ROOT + "/dir2/file2_2.h",
    };

    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);

    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_2.cpp"), file1_2_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_2.h"), file1_2_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_1.h"), file2_1_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_2.h"), file2_2_h_deps);
}

TEST(GccReaderTest, read_one_file_4) {
    const GccReader reader(FILES_ROOT, FILES_ROOT + "/dir1/file1_3.cpp", "-std=c++14 -I " + FILES_ROOT);
    const auto graph = reader.read();
    const nodes_T expected_nodes = {
        FILES_ROOT + "/dir1/file1_3.cpp",
        FILES_ROOT + "/dir1/file1_1.h",
        FILES_ROOT + "/dir1/file1_2.h",
        FILES_ROOT + "/dir2/file2_1.h",
        FILES_ROOT + "/dir2/file2_2.h",
    };

    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);

    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_3.cpp"), file1_3_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_1.h"), file1_1_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_2.h"), file1_2_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_1.h"), file2_1_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_2.h"), file2_2_h_deps);
}

TEST(GccReaderTest, read_two_files) {
    const GccReader reader(FILES_ROOT,
          {
                FILES_ROOT + "/dir1/file1_1.cpp",
                FILES_ROOT + "/dir1/file1_2.cpp",
          },
          "-std=c++14 -I " + FILES_ROOT);
    const auto graph = reader.read();
    const nodes_T expected_nodes = {
        FILES_ROOT + "/dir1/file1_1.cpp",
        FILES_ROOT + "/dir1/file1_2.cpp",
        FILES_ROOT + "/dir1/file1_1.h",
        FILES_ROOT + "/dir1/file1_2.h",
        FILES_ROOT + "/dir2/file2_1.h",
        FILES_ROOT + "/dir2/file2_2.h",
    };

    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);

    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_1.cpp"), file1_1_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_2.cpp"), file1_2_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir1/file1_1.h"), file1_1_h_deps);
}

namespace {
void verify_gcc_error(const node_T& full_path, const std::string& options) {
    const GccReader reader(FILES_ROOT, full_path, options);
    EXPECT_THROW(reader.read(), std::runtime_error) << full_path << " / " << options;
}
}

TEST(GccReaderTest, gcc_error) {
    verify_gcc_error("non_existent.cpp", "-std=c++14");
    verify_gcc_error(FILES_ROOT + "/dir1/file1_1.cpp", "-std=c++14"); // no -I
    verify_gcc_error(FILES_ROOT + "/dir1/file1_1.cpp", "-std=blah -I " + FILES_ROOT);
}

TEST(GccReaderTest, read_one_file_with_inl) {
    const GccReader reader(FILES_ROOT, FILES_ROOT + "/dir2/file2_1.cpp", "-std=c++14 -I " + FILES_ROOT);
    const auto graph = reader.read();
    const nodes_T expected_nodes = {
        FILES_ROOT + "/dir2/file2_1.cpp",
        FILES_ROOT + "/dir2/templates.inl",
        FILES_ROOT + "/dir2/file2_1.h",
        FILES_ROOT + "/dir2/file2_2.h",
    };

    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);

    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_1.cpp"), file2_1_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/templates.inl"), nodes_T());
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_1.h"), file2_1_h_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_2.h"), file2_2_h_deps);
}

TEST(GccReaderTest, read_one_file_with_relative_path) {
    const GccReader reader(FILES_ROOT, FILES_ROOT + "/dir2/relative.cpp", "-std=c++14 -I " + FILES_ROOT);
    const auto graph = reader.read();
    const nodes_T expected_nodes = {
        FILES_ROOT + "/dir2/file2_2.h",
        FILES_ROOT + "/dir2/relative.cpp",
    };

    const auto nodes = graph.nodes();
    EXPECT_EQ(nodes, expected_nodes);

    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/relative.cpp"), relative_cpp_deps);
    EXPECT_EQ(graph.dependencies(FILES_ROOT + "/dir2/file2_2.h"), file2_2_h_deps);
}

//TEST(GccReaderTest, compare_to_helper) {
//    const std::string file = FILES_ROOT + "/dir1/file1_3.cpp";
//    const std::string options = "-std=c++17 -I " + FILES_ROOT;
//
//    const GccReader reader(FILES_ROOT, file, options);
//    const auto graph = reader.read();
//    dependency::DependencyGraph expected_graph;
//    impl::process_file(expected_graph, FILES_ROOT, file, options);
//
//    EXPECT_EQ(graph, expected_graph);
//    EXPECT_EQ(graph.nodes(), expected_graph.nodes());
//
//}
}
