#include <controller/DependencyGraph.h>

#include <gtest/gtest.h>

namespace dependency {

TEST(DependencyGraphTest, create) {
    const DependencyGraph graph;
    EXPECT_TRUE(graph.nodes().empty());
}

namespace {
bool exists(const std::string& node, const nodes_T& nodes) {
    return std::find(nodes.begin(), nodes.end(), node) != nodes.end();
}
}

TEST(DependencyGraphTest, add_node) {
    DependencyGraph graph;
    graph.add("node 1");
    EXPECT_FALSE(graph.nodes().empty());
    EXPECT_TRUE(graph.dependencies("node 1").empty());
    EXPECT_EQ(graph.nodes().size(), 1u);
    EXPECT_TRUE(exists("node 1", graph.nodes()));
}

TEST(DependencyGraphTest, add_node_duplicated) {
    DependencyGraph graph;
    graph.add("node 1");
    graph.add("node 1");
    EXPECT_FALSE(graph.nodes().empty());
    EXPECT_TRUE(graph.dependencies("node 1").empty());
    EXPECT_EQ(graph.nodes().size(), 1u);
    EXPECT_TRUE(exists("node 1", graph.nodes()));
}

TEST(DependencyGraphTest, add_two_nodes) {
    DependencyGraph graph;
    graph.add("node 1");
    graph.add("node 2");
    EXPECT_FALSE(graph.nodes().empty());
    EXPECT_TRUE(graph.dependencies("node 1").empty());
    EXPECT_TRUE(graph.dependencies("node 2").empty());
    EXPECT_EQ(graph.nodes().size(), 2u);
    EXPECT_TRUE(exists("node 1", graph.nodes()));
    EXPECT_TRUE(exists("node 2", graph.nodes()));
}

TEST(DependencyGraphTest, add_nodes_with_dependencies) {
    DependencyGraph graph;
    graph.add("node 1", { "node 3", "node 4" });
    graph.add("node 2", { "node 5" });
    EXPECT_FALSE(graph.nodes().empty());
    EXPECT_FALSE(graph.dependencies("node 1").empty());
    EXPECT_FALSE(graph.dependencies("node 2").empty());
    EXPECT_TRUE(graph.dependencies("node 3").empty());
    EXPECT_TRUE(graph.dependencies("node 4").empty());
    EXPECT_TRUE(graph.dependencies("node 5").empty());
    EXPECT_EQ(graph.nodes().size(), 5u);
    EXPECT_TRUE(exists("node 1", graph.nodes()));
    EXPECT_TRUE(exists("node 2", graph.nodes()));
    EXPECT_TRUE(exists("node 3", graph.nodes()));
    EXPECT_TRUE(exists("node 4", graph.nodes()));
    EXPECT_TRUE(exists("node 5", graph.nodes()));
}

namespace {
void check_dependencies(int line,
      const DependencyGraph& graph,
      const node_T& name,
      const nodes_T& expected) {
    EXPECT_EQ(graph.dependencies(name), expected) << name << " from line " << line;
}
}
TEST(DependencyGraphTest, add_with_dependency) {
    DependencyGraph graph;
    graph.add("node 1", { "node 3", "node 4" });
    graph.add("node 2");
    EXPECT_FALSE(graph.nodes().empty());
    EXPECT_FALSE(graph.dependencies("node 1").empty());
    EXPECT_TRUE(graph.dependencies("node 2").empty());
    EXPECT_EQ(graph.nodes().size(), 4u);
    const auto nodes = graph.nodes();
    EXPECT_TRUE(exists("node 1", nodes));
    EXPECT_TRUE(exists("node 2", nodes));
    EXPECT_TRUE(exists("node 3", nodes));
    EXPECT_TRUE(exists("node 4", nodes));

    check_dependencies(__LINE__, graph, "node 1", { "node 3", "node 4" });
    check_dependencies(__LINE__, graph, "node 2", nodes_T());
    check_dependencies(__LINE__, graph, "node 3", nodes_T());
    check_dependencies(__LINE__, graph, "node 4", nodes_T());
}

TEST(DependencyGraphTest, change) {
    DependencyGraph graph;
    graph.add("node 1", { "node 3", "node 4" });
    graph.add("node 2");
    graph.change([](const node_T& node) { return node + " something"; });
    const auto nodes = graph.nodes();
    EXPECT_EQ(graph.nodes().size(), 4u);
    EXPECT_TRUE(exists("node 1 something", nodes));
    EXPECT_TRUE(exists("node 2 something", nodes));
    EXPECT_TRUE(exists("node 3 something", nodes));
    EXPECT_TRUE(exists("node 4 something", nodes));
}

TEST(DependencyGraphTest, remove) {
    DependencyGraph graph;
    graph.add("node 1", { "node 2", "node 3", "node 4" });
    graph.add("node 2", { "node 6", "node 7", "node 8" });
    graph.add("node 3", { "node 7", "node 5", "node 2" });
    graph.add("node 9", { "node 1", "node 2", "node 3" });

    EXPECT_EQ(graph.nodes().size(), 9u);
    check_dependencies(__LINE__, graph, "node 1", { "node 2", "node 3", "node 4" });
    check_dependencies(__LINE__, graph, "node 2", { "node 6", "node 7", "node 8" });
    check_dependencies(__LINE__, graph, "node 3", { "node 7", "node 5", "node 2" });
    check_dependencies(__LINE__, graph, "node 4", nodes_T());
    check_dependencies(__LINE__, graph, "node 5", nodes_T());
    check_dependencies(__LINE__, graph, "node 6", nodes_T());
    check_dependencies(__LINE__, graph, "node 7", nodes_T());
    check_dependencies(__LINE__, graph, "node 8", nodes_T());
    check_dependencies(__LINE__, graph, "node 9", { "node 1", "node 2", "node 3" });

    graph.remove("node 6");

    EXPECT_EQ(graph.nodes().size(), 8u);
    check_dependencies(__LINE__, graph, "node 1", { "node 2", "node 3", "node 4" });
    check_dependencies(__LINE__, graph, "node 2", { "node 7", "node 8" });
    check_dependencies(__LINE__, graph, "node 3", { "node 7", "node 5", "node 2" });
    check_dependencies(__LINE__, graph, "node 4", nodes_T());
    check_dependencies(__LINE__, graph, "node 5", nodes_T());
    EXPECT_FALSE(exists("node 6", graph.nodes()));
    check_dependencies(__LINE__, graph, "node 6", nodes_T());
    check_dependencies(__LINE__, graph, "node 7", nodes_T());
    check_dependencies(__LINE__, graph, "node 8", nodes_T());
    check_dependencies(__LINE__, graph, "node 9", { "node 1", "node 2", "node 3" });

    graph.remove("node 2");

    EXPECT_EQ(graph.nodes().size(), 7u);
    check_dependencies(__LINE__, graph, "node 1", { "node 3", "node 4" });
    EXPECT_FALSE(exists("node 2", graph.nodes()));
    check_dependencies(__LINE__, graph, "node 3", { "node 7", "node 5" });
    check_dependencies(__LINE__, graph, "node 4", nodes_T());
    check_dependencies(__LINE__, graph, "node 5", nodes_T());
    EXPECT_FALSE(exists("node 6", graph.nodes()));
    check_dependencies(__LINE__, graph, "node 6", nodes_T());
    check_dependencies(__LINE__, graph, "node 7", nodes_T());
    check_dependencies(__LINE__, graph, "node 8", nodes_T());
    check_dependencies(__LINE__, graph, "node 9", { "node 1", "node 3" });

    graph.remove("node 4");
    graph.remove("node 5");
    graph.remove("node 7");
    graph.remove("node 3");
    graph.remove("node 1");
    graph.remove("node 6");
    graph.remove("node 8");
    graph.remove("node 9");

    EXPECT_EQ(graph.nodes().size(), 0u);
}

TEST(DependencyGraphTest, remove_if) {
    DependencyGraph graph;
    graph.add("node 1", { "node 2", "node 3", "node 4" });
    graph.add("node 2", { "node 6", "node 7", "node 8" });
    graph.add("node 3", { "node 7", "node 5", "node 2" });
    graph.add("node 9", { "node 1", "node 2", "node 3" });

    EXPECT_EQ(graph.nodes().size(), 9u);
    check_dependencies(__LINE__, graph, "node 1", { "node 2", "node 3", "node 4" });
    check_dependencies(__LINE__, graph, "node 2", { "node 6", "node 7", "node 8" });
    check_dependencies(__LINE__, graph, "node 3", { "node 7", "node 5", "node 2" });
    check_dependencies(__LINE__, graph, "node 4", nodes_T());
    check_dependencies(__LINE__, graph, "node 5", nodes_T());
    check_dependencies(__LINE__, graph, "node 6", nodes_T());
    check_dependencies(__LINE__, graph, "node 7", nodes_T());
    check_dependencies(__LINE__, graph, "node 8", nodes_T());
    check_dependencies(__LINE__, graph, "node 9", { "node 1", "node 2", "node 3" });

    graph.remove_if([](const node_T& node_name) { return node_name.back() == '6'; });
    EXPECT_EQ(graph.nodes().size(), 8u);
    check_dependencies(__LINE__, graph, "node 1", { "node 2", "node 3", "node 4" });
    check_dependencies(__LINE__, graph, "node 2", { "node 7", "node 8" });
    check_dependencies(__LINE__, graph, "node 3", { "node 7", "node 5", "node 2" });
    check_dependencies(__LINE__, graph, "node 4", nodes_T());
    check_dependencies(__LINE__, graph, "node 5", nodes_T());
    EXPECT_FALSE(exists("node 6", graph.nodes()));
    check_dependencies(__LINE__, graph, "node 6", nodes_T());
    check_dependencies(__LINE__, graph, "node 7", nodes_T());
    check_dependencies(__LINE__, graph, "node 8", nodes_T());
    check_dependencies(__LINE__, graph, "node 9", { "node 1", "node 2", "node 3" });

    graph.remove_if([](const node_T& node_name) { return node_name.back() == '2'; });

    EXPECT_EQ(graph.nodes().size(), 7u);
    check_dependencies(__LINE__, graph, "node 1", { "node 3", "node 4" });
    EXPECT_FALSE(exists("node 2", graph.nodes()));
    check_dependencies(__LINE__, graph, "node 3", { "node 7", "node 5" });
    check_dependencies(__LINE__, graph, "node 4", nodes_T());
    check_dependencies(__LINE__, graph, "node 5", nodes_T());
    EXPECT_FALSE(exists("node 6", graph.nodes()));
    check_dependencies(__LINE__, graph, "node 6", nodes_T());
    check_dependencies(__LINE__, graph, "node 7", nodes_T());
    check_dependencies(__LINE__, graph, "node 8", nodes_T());
    check_dependencies(__LINE__, graph, "node 9", { "node 1", "node 3" });

    graph.remove_if([](const node_T&) { return true; });

    EXPECT_EQ(graph.nodes().size(), 0u);
}

TEST(DependencyGraphTest, exist) {
    DependencyGraph graph;
    graph.add("node 1", { "node 2", "node 3", "node 4" });
    graph.add("node 2", { "node 6", "node 7", "node 8" });
    graph.add("node 3", { "node 7", "node 5", "node 2" });
    graph.add("node 9", { "node 1", "node 2", "node 3" });

    EXPECT_TRUE(graph.exist("node 1"));
    EXPECT_TRUE(graph.exist("node 2"));
    EXPECT_TRUE(graph.exist("node 3"));
    EXPECT_TRUE(graph.exist("node 4"));
    EXPECT_TRUE(graph.exist("node 5"));
    EXPECT_TRUE(graph.exist("node 6"));
    EXPECT_TRUE(graph.exist("node 7"));
    EXPECT_TRUE(graph.exist("node 8"));
    EXPECT_TRUE(graph.exist("node 9"));

    EXPECT_FALSE(graph.exist("node 0"));
    EXPECT_FALSE(graph.exist("NODE 1"));
}
} // namespace dependency
