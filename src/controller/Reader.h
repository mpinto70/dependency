#pragma once

#include <controller/DependencyGraph.h>

namespace dependency {

class Reader {
public:
    virtual ~Reader() = default;

    virtual DependencyGraph read() const = 0;
};
}
