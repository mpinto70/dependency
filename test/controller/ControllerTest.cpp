#include <controller/Controller.h>

#include <gtest/gtest.h>

namespace dependency {
namespace {
class ReaderMock : public Reader {
public:
    explicit ReaderMock(const DependencyGraph& graph)
          : graph_(graph) {
    }

    DependencyGraph read() const override {
        return graph_;
    }

private:
    DependencyGraph graph_;
};

class ChangerMock : public Changer {
public:
    explicit ChangerMock(const DependencyGraph& graph)
          : graph_(graph) {
    }

    void change(DependencyGraph& graph) const override {
        in_graph_ = graph;
        graph = graph_;
    }

    const DependencyGraph& in_graph() const { return in_graph_; }

private:
    mutable DependencyGraph in_graph_;
    DependencyGraph graph_;
};

class WriterMock : public Writer {
public:
    void write(std::ostream& out,
          const DependencyGraph& graph,
          const std::vector<Highlight>& highlights) const override {
        out << "called\n";
        graph_ = graph;
        highlights_ = highlights;
    }

    const DependencyGraph& graph() const { return graph_; }
    const std::vector<Highlight>& highlights() const { return highlights_; }

private:
    mutable DependencyGraph graph_;
    mutable std::vector<Highlight> highlights_;
};
}

TEST(ControllerTest, run) {
    DependencyGraph graph_read;
    graph_read.add("A", { "B" });
    graph_read.add("C", { "D" });

    std::vector<DependencyGraph> graph_changes(2, DependencyGraph());
    graph_changes[0].add("D", { "E" });
    graph_changes[0].add("F", { "G" });
    graph_changes[1].add("H", { "I" });
    std::ostringstream out;
    std::unique_ptr<Reader> reader = std::make_unique<ReaderMock>(graph_read);
    std::vector<std::unique_ptr<Changer>> changers;
    changers.emplace_back(std::make_unique<ChangerMock>(graph_changes[0]));
    changers.emplace_back(std::make_unique<ChangerMock>(graph_changes[1]));
    std::unique_ptr<Writer> writer = std::make_unique<WriterMock>();

    Controller::run(reader, changers, writer, {}, false, out);
    EXPECT_EQ(dynamic_cast<const ChangerMock&>(*changers[0]).in_graph(), graph_read);
    EXPECT_EQ(dynamic_cast<const ChangerMock&>(*changers[1]).in_graph(), graph_changes[0]);
    EXPECT_EQ(dynamic_cast<const WriterMock&>(*writer).graph(), graph_changes[1]);
    EXPECT_EQ(out.str(), "called\n");
}
}
