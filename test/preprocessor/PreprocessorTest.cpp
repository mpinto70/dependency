#include <preprocessor/Preprocessor.h>

#include <gtest/gtest.h>

#include <getopt.h>

#include <cstring>
#include <string>

namespace preprocessor {

namespace {
template <size_t BUFFER_SIZE, size_t ARGV_SIZE>
void build(const options_t& options,
      char (&buffer)[BUFFER_SIZE],
      int& argc,
      char* (&argv)[ARGV_SIZE]) {
    ASSERT_LT(options.size(), ARGV_SIZE);
    memset(buffer, 0, BUFFER_SIZE);
    memset(argv, 0, ARGV_SIZE * sizeof(char*));
    argc = 0;
    size_t curr_idx = 0;
    for (size_t i = 0; i < options.size(); ++i) {
        const auto& option = options[i];
        ASSERT_LT(curr_idx + option.size() + 1, BUFFER_SIZE) << option;
        argv[i] = strcpy(&buffer[curr_idx], option.c_str());
        curr_idx += option.size() + 1;
        ++argc;
    }
    optind = 0;
}
}

TEST(PreprocessorTest, create_general) {
    try {
        const options_t options = {
            "command",
            "/path/to/project",
            "file1.cpp",
            "relative/path/file2.cpp",
            "/full/path/file3.cpp",
            "-i",
            "ignore1",
            "-i",
            "ignore2",
            "--ignore=ignore3",
            "-c",
            "cut1",
            "-c",
            "cut2",
            "-c",
            "cut3",
            "--cut=cut4",
            "-o",
            "-one option",
            "--option=--other option",
            "--highlight=highlight 1",
            "-H",
            "highlight 2",
        };
        int argc;
        char* argv[50];
        char buffer[4096];
        build(options, buffer, argc, argv);
        const Preprocessor pre = Preprocessor::create(options.size(), argv);
        const options_t expected_files = {
            "file1.cpp",
            "relative/path/file2.cpp",
            "/full/path/file3.cpp",
        };
        const options_t expected_ignores = {
            "ignore1",
            "ignore2",
            "ignore3",
        };
        const options_t expected_cuts = {
            "cut1",
            "cut2",
            "cut3",
            "cut4",
        };
        const options_t expected_options = {
            "-one option",
            "--other option",
        };
        const options_t expected_highlights = {
            "highlight 1",
            "highlight 2",
        };

        EXPECT_EQ(pre.project_path(), "/path/to/project");
        EXPECT_EQ(pre.files(), expected_files);
        EXPECT_EQ(pre.ignores(), expected_ignores);
        EXPECT_EQ(pre.cuts(), expected_cuts);
        EXPECT_EQ(pre.options(), expected_options);
        EXPECT_EQ(pre.collapse_type(), CollapseType::None);
        EXPECT_EQ(pre.highlights(), expected_highlights);
    } catch (std::exception& e) {
        FAIL() << e.what();
    }
}

namespace {
void check_collapse(const options_t& options, CollapseType expected) {
    int argc;
    char* argv[50];
    char buffer[4096];

    build(options, buffer, argc, argv);
    const Preprocessor pre = Preprocessor::create(options.size(), argv);
    EXPECT_EQ(pre.collapse_type(), expected);
}
}

TEST(PreprocessorTest, collapse_file) {
    try {
        check_collapse({ "command", "/path", "file1.cpp" }, CollapseType::None);
        check_collapse({ "command", "/path", "file1.cpp", "-s" }, CollapseType::File);
        check_collapse({ "command", "/path", "file1.cpp", "--collapse_file" }, CollapseType::File);
        check_collapse({ "command", "/path", "file1.cpp", "-d" }, CollapseType::Directory);
        check_collapse({ "command", "/path", "file1.cpp", "--collapse_dir" }, CollapseType::Directory);
    } catch (std::exception& e) {
        FAIL() << e.what();
    }
}

namespace {
void check_highlight(const options_t& options, const options_t& expected) {
    int argc;
    char* argv[50];
    char buffer[4096];

    build(options, buffer, argc, argv);
    const Preprocessor pre = Preprocessor::create(options.size(), argv);
    EXPECT_EQ(pre.highlights(), expected);
}
}

TEST(PreprocessorTest, highlight) {
    try {
        check_highlight({ "command", "/path", "file.cpp", "-H", "text" }, { "text" });
        check_highlight({ "command", "/path", "file.cpp", "--highlight=text" }, { "text" });
        check_highlight({ "command", "/path", "file.cpp", "-H", "text 1", "--highlight=text 2" }, { "text 1", "text 2" });
    } catch (std::exception& e) {
        FAIL() << e.what();
    }
}

namespace {
void check_auto_highlight(const options_t& options, AutoHighlight expected) {
    int argc;
    char* argv[50];
    char buffer[4096];

    build(options, buffer, argc, argv);
    const Preprocessor pre = Preprocessor::create(options.size(), argv);
    EXPECT_EQ(pre.auto_highlight(), expected);
}
}

TEST(PreprocessorTest, auto_highlight) {
    try {
        check_auto_highlight({ "command", "/path", "file1.cpp" }, AutoHighlight::No);
        check_auto_highlight({ "command", "/path", "file1.cpp", "-A" }, AutoHighlight::Yes);
        check_auto_highlight({ "command", "/path", "file1.cpp", "-H", "text" }, AutoHighlight::No);
        check_auto_highlight({ "command", "/path", "file1.cpp", "-H", "text", "-A" }, AutoHighlight::Yes);
    } catch (std::exception& e) {
        FAIL() << e.what();
    }
}
}
