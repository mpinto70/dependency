set(util_srcs
    UtilTest.cpp
)

set(util_libs
    util
)

add_unit_test(
    util
    util_srcs
    util_libs
)
