#pragma once

#include <controller/DependencyGraph.h>

#include <ostream>

namespace dependency {

class Writer {
public:
    virtual ~Writer() = default;

    virtual void write(std::ostream& out,
          const DependencyGraph& graph,
          const std::vector<Highlight>& highlights) const = 0;
};
}
