# dependency

This is a simple dependency graph generator to help diagnose C++ applications.

## Building

1. checkout the repository: `git clone git@bitbucket.org:mpinto70/dependency.git`
1. build the system (from `dependency` root): `./runbuild`
1. test the system (from `dependency` root): `./runbuild test`
1. demo the system (from `dependency` root): `./runbuild demo`

### Dependencies

In order to build the system, you need to install:
`sudo apt-get install -y g++ cmake cppcheck graphviz`

## Running the system

In this section, I assume you are in `dependency` root directory and you already successfully built it.

If you run the system with `./bin/dependency -h` you will see the following help:

```
$ bin/dependency -h
Program that build a dependency graph for C++ projects

bin/dependency FILE|DIR... <options>

Positional parameters:
    FILE|DIR...             name of files or directories to process
Options:
    -i, --ignore  <text>    text that will be used to ignore files that match it
    -c, --cut  <text>       text that will be removed from file path
    -o, --option  <text>    compiler option (forwarded)
    -H, --highlight  <text> text in cell that cause it to be highlighted (see below)
    -A, --auto_highlight    if the system should auto highlight (see below)
    -s, --collapse_file     if .h and .cpp should be treated as components
    -d, --collapse_dir      if directories should be treated as nodes

Highlight and Auto Highlight
  Highlight option (-H) accepts either
    * <text> in witch case it will highlight in blue over lightcyan
    * <text,foreground,background> in which case it will highlight in foreground over background
    highlights are applied in the order of their definition in the command line
  Auto highlight option (-A) makes the highlight apply to each distinct directory for the nodes
    that have no other highlight specified by a -H option
  Please, check https://www.graphviz.org/doc/info/colors.html for the possible colors
```

One example running the system is (check content of `demo` target in `runbuild` script):

```bash
set -e
dependency_dir=$(pwd)
${dependency_dir}/bin/dependency \
    ${dependency_dir}/src \
    -o "-I ${dependency_dir}/src --std=c++14" \
    -c ${dependency_dir} \
    -s \
    -H dependency,#212121,#fdb81e \
    -H controller,white,#2e8540 \
    -H reader,white,#981b1e \
    -H preprocessor,white,#112e51 \
    -H util,white,#205493 \
    -H writer \
    > depend.dot

dot -Tsvg depend.dot -o depend.svg

eog depend.svg
```

Note that the system outputs to `stdout` a graph specification using [graphviz](https://www.graphviz.org/) dot language.
After redirecting the output to a file, you are able to run `dot` on it to get any type of image and then use any
visualizer you want.

## Color suggestions

Read [this page](https://www.graphviz.org/doc/info/colors.html) for available color names. Note that you can use
hexadecimal RGB codes as well (e.g., `#fdb81e`). For a beautiful palette, please check
[this page](https://designsystem.digital.gov/components/colors/)

![suggestion](https://via.placeholder.com/250x50/fdb81e/212121?text=sample) #212121,#fdb81e

![suggestion](https://via.placeholder.com/250x50/2e8540/ffffff?text=sample) white,#2e8540

![suggestion](https://via.placeholder.com/250x50/981b1e/ffffff?text=sample) white,#981b1e

![suggestion](https://via.placeholder.com/250x50/112e51/ffffff?text=sample) white,#112e51

![suggestion](https://via.placeholder.com/250x50/205493/ffffff?text=sample) white,#205493

