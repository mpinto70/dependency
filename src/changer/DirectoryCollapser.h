#pragma once

#include <controller/Changer.h>

namespace changer {

class DirectoryCollapser : public dependency::Changer {
public:
    ~DirectoryCollapser() override = default;
    void change(dependency::DependencyGraph& graph) const override;
};
}
