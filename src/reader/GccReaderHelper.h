#pragma once

#include <controller/DependencyGraph.h>

#include <string>

namespace reader {
namespace impl {

struct Pipe {
    enum Side {
        read,
        write
    };

    Pipe();
    ~Pipe();

    Pipe(const Pipe& x) = delete;
    Pipe(Pipe&& x) noexcept;
    Pipe& operator=(const Pipe& x) = delete;
    Pipe& operator=(Pipe&& x) noexcept;

    void close(Side i) noexcept;

    int operator[](Side i) const noexcept {
        return fd_[i];
    }

private:
    static constexpr int NO_FILE = -1;
    int fd_[2];
};

dependency::nodes_T process_file(const std::string& full_path_root,
      const dependency::node_T& full_path,
      const std::string& gcc_options);

std::string run_gcc_command(const dependency::node_T& full_path, const std::string& gcc_options);

void run_gcc_child(const std::string& full_path,
      const std::string& gcc_options,
      Pipe& fd_out,
      Pipe& fd_err);

std::string read_gcc_output(Pipe& fd_out, Pipe& fd_err, pid_t pid);

std::vector<std::string> get_dependency_tree_from(const std::string& gcc_output);

dependency::nodes_T process_dependency(const std::string& full_path_root,
      const dependency::node_T& node,
      const std::vector<std::string>& dependency_lines);

dependency::node_T full_path_of(const std::string& root, const dependency::node_T& node);

}
}
