set(controller_srcs
    ControllerTest.cpp
    DependencyGraphTest.cpp
    HighlightTest.cpp
)

set(controller_libs
    controller
    util
)

add_unit_test(
    controller
    controller_srcs
    controller_libs
)
