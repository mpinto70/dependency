#pragma once

#include <string>
#include <vector>

namespace util {
bool is_valid_file(const std::string& file);
std::vector<std::string> split(const std::string& str, char sep);
std::string trim(std::string str, char blank = ' ');
bool starts_with(const std::string& str, const std::string& start);
std::string join(const std::vector<std::string>& tokens, char joiner);
}
