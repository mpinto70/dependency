#include <preprocessor/PathResolver.h>
#include <util/Util.h>

#include <algorithm>
#include <filesystem>
#include <stdexcept>

namespace preprocessor {
namespace fs = std::filesystem;

std::string PathResolver::canonicalize(const std::string& path) {
    const auto relative = fs::path(path);
    if (not fs::exists(relative))
        throw std::runtime_error("path not found " + path);
    return fs::canonical(relative).string();
}

std::vector<std::string> PathResolver::resolve_dir(const std::string& path) {
    std::vector<std::string> res;
    for (fs::recursive_directory_iterator end, entry_it(path); entry_it != end; ++entry_it) {
        const auto& entry = entry_it->path();
        if (fs::is_regular_file(entry)) {
            if (util::is_valid_file(entry.string())) {
                res.push_back(fs::canonical(entry).string());
            }
        }
    }
    return res;
}

std::vector<std::string> PathResolver::resolve(const std::vector<std::string>& paths) {
    std::vector<std::string> res;
    for (const auto& path : paths) {
        if (fs::is_regular_file(path)) {
            res.push_back(path);
        } else if (fs::is_directory(path)) {
            const auto files = resolve_dir(path);
            res.insert(res.end(), files.begin(), files.end());
        }
    }
    std::transform(res.begin(), res.end(), res.begin(), canonicalize);
    return res;
}
}
