#include <reader/GccReaderHelper.h>
#include <util/Util.h>

#include <sys/wait.h>
#include <unistd.h>

#include <cstring>
#include <filesystem>
#include <future>
#include <iostream>
#include <stdexcept>

namespace reader {
namespace impl {
namespace {
void throw_strerror(const std::string& base_msg) {
    throw std::runtime_error(base_msg + ": " + ::strerror(errno));
}

void duplicate2(int fd, int fd_target) {
    if (::dup2(fd, fd_target) != fd_target) {
        std::cerr << "Error duplicating fd(" << fd << ") to (" << fd_target << ")\n";
        std::exit(1);
    }
}

std::string read_content(int fd) {
    int n;
    constexpr size_t BUFFER_SIZE = 4096;
    char line[BUFFER_SIZE];
    std::string output;
    while ((n = ::read(fd, line, BUFFER_SIZE)) > 0) {
        output += std::string(line, n);
    }
    return output;
}

std::vector<std::string> split_string(const std::string& str, char sep) {
    if (str.empty()) {
        return {};
    }
    std::vector<std::string> tokens = util::split(str, sep);

    // trim all tokens
    std::transform(tokens.begin(), tokens.end(), tokens.begin(), [](const auto& t) { return util::trim(t); });

    // remove empty tokens
    tokens.erase(std::remove_if(tokens.begin(), tokens.end(), [](const auto& token) { return token.empty(); }), tokens.end());

    if (tokens.empty()) {
        throw std::invalid_argument("GccReader - invalid string to split [" + str + "]");
    }

    return tokens;
}

bool file_in_path(const std::string& file_path, const std::string& dir_path) {
    return file_path.size() >= dir_path.size() && file_path.compare(0, dir_path.size(), dir_path) == 0;
}
}

Pipe::Pipe()
      : fd_{} {
    if (::pipe(fd_) != 0) {
        throw_strerror("Unable to create pipe");
    }
}

Pipe::~Pipe() {
    close(read);
    close(write);
}

Pipe::Pipe(Pipe&& x) noexcept {
    fd_[read] = x.fd_[read];
    fd_[write] = x.fd_[write];
    x.fd_[read] = NO_FILE;
    x.fd_[write] = NO_FILE;
}

Pipe& Pipe::operator=(Pipe&& x) noexcept {
    fd_[read] = x.fd_[read];
    fd_[write] = x.fd_[write];
    x.fd_[read] = NO_FILE;
    x.fd_[write] = NO_FILE;
    return *this;
}

void Pipe::close(Side i) noexcept {
    if (fd_[i] != NO_FILE) {
        ::close(fd_[i]);
        fd_[i] = NO_FILE;
    }
}
}

dependency::nodes_T impl::process_file(const std::string& full_path_root,
      const dependency::node_T& full_path,
      const std::string& gcc_options) {
    auto output = run_gcc_command(full_path, gcc_options);
    const auto tree = get_dependency_tree_from(output);

    return process_dependency(full_path_root, full_path, tree);
}

std::string impl::run_gcc_command(const dependency::node_T& full_path, const std::string& gcc_options) {
    Pipe fd_out, fd_err;
    const pid_t pid = ::fork();
    switch (pid) {
        case -1:
            throw_strerror("Unable to create pipe to run gcc");
            break;
        case 0: // child
            fd_out.close(Pipe::read);
            fd_err.close(Pipe::read);
            run_gcc_child(full_path, gcc_options, fd_out, fd_err);
            std::exit(1);
        default: // parent
            fd_out.close(Pipe::write);
            fd_err.close(Pipe::write);
            break;
    }
    return read_gcc_output(fd_out, fd_err, pid);
}

std::string impl::read_gcc_output(Pipe& fd_out, Pipe& fd_err, pid_t pid) {
    std::future<std::string> out_future = std::async(std::launch::async, read_content, fd_out[Pipe::read]);
    std::future<std::string> err_future = std::async(std::launch::async, read_content, fd_err[Pipe::read]);
    out_future.get();
    const std::string error = err_future.get();
    fd_out.close(Pipe::read);
    fd_err.close(Pipe::read);

    int status;

    if (waitpid(pid, &status, 0) != -1) {
        if (WIFEXITED(status)) {
            const int returned = WEXITSTATUS(status);
            if (returned != 0) {
                throw std::runtime_error("gcc exited with status " + std::to_string(returned)
                                         + " and error message: " + error);
            }
        } else if (WIFSIGNALED(status)) {
            const int signum = WTERMSIG(status);
            throw std::runtime_error("gcc was interrupted by signal " + std::to_string(signum));
        } else {
            throw std::runtime_error("something unexpected happened while running gcc");
        }
    } else {
        throw_strerror("Waiting for child (" + std::to_string(pid) + ")");
    }
    return error;
}

void impl::run_gcc_child(const std::string& full_path,
      const std::string& gcc_options,
      Pipe& fd_out,
      Pipe& fd_err) {
    const auto cmd = "g++ -M -H " + gcc_options + " " + full_path + " -MT " + full_path;
    std::vector<std::string> tokens = util::split(cmd, ' ');
    tokens.erase(std::remove(tokens.begin(), tokens.end(), ""), tokens.end());

    std::vector<char*> argv;
    argv.reserve(tokens.size() + 1);
    std::transform(tokens.begin(), tokens.end(), std::back_inserter(argv), [](std::string& token) {
        return &token[0];
    });
    argv.push_back(nullptr);

    duplicate2(fd_out[Pipe::write], STDOUT_FILENO);
    duplicate2(fd_err[Pipe::write], STDERR_FILENO);
    fd_out.close(Pipe::write);
    fd_err.close(Pipe::write);

    ::execvp("g++", &argv[0]);
    std::cerr << "execvp failed for [" << cmd << "]\n";
    std::exit(1);
}

std::vector<std::string> impl::get_dependency_tree_from(const std::string& gcc_output) {
    std::vector<std::string> tree = split_string(gcc_output, '\n');
    tree.erase(std::remove_if(tree.begin(), tree.end(), [](const auto& str) { return str.size() == 0 || str[0] != '.'; }), tree.end());
    return tree;
}

dependency::nodes_T impl::process_dependency(const std::string& full_path_root,
      const dependency::node_T& node,
      const std::vector<std::string>& dependency_lines) {
    using dependency::node_T;
    using dependency::nodes_T;
    namespace fs = std::filesystem;
    const auto node_path = fs::path(node).remove_filename();
    nodes_T res;
    for (const auto& line : dependency_lines) {
        // line has to start with a group of dots (.)
        const size_t space_pos = line.find_first_not_of('.');
        if (space_pos == std::string::npos || space_pos == 0) {
            throw std::runtime_error("Invalid line in dependency tree: " + line);
        }
        if (space_pos != 1) {
            continue;
        }
        const node_T current_node = full_path_of(node_path, line.substr(space_pos + 1));

        if (file_in_path(current_node, full_path_root)) {
            res.insert(current_node);
        }
    }

    return res;
}

dependency::node_T impl::full_path_of(const std::string& root, const dependency::node_T& node) {
    namespace fs = std::filesystem;
    if (fs::path(node).has_root_directory()) {
        return fs::weakly_canonical(node);
    } else {
        return fs::weakly_canonical(fs::path(root) / fs::path(node));
    }
}

}
