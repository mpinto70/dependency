#include <changer/Remover.h>

#include <algorithm>

namespace changer {

using dependency::DependencyGraph;
using dependency::node_T;

Remover::Remover(const std::vector<std::string>& masks)
      : masks_(masks) {
}

void Remover::change(DependencyGraph& graph) const {
    graph.remove_if([&](const node_T& node_name) {
        for (const auto& mask : masks_) {
            if (node_name.find(mask) != std::string::npos) {
                return true;
            }
        }
        return false;
    });
}
}
