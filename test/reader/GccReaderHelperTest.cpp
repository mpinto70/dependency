#include <reader/GccReaderHelper.h>

#include <controller/DependencyGraph.h>

#include <gtest/gtest.h>

namespace reader {
namespace impl {
TEST(GccReaderHelperTest, UniquePipe_opens_pipe) {
    Pipe pipe;
    EXPECT_GT(pipe[Pipe::read], 0);
    EXPECT_GT(pipe[Pipe::write], 0);

    const char buffer_write[] = "some string in one end";
    char buffer_read[sizeof(buffer_write)] = {};

    EXPECT_EQ(write(pipe[Pipe::write], buffer_write, sizeof(buffer_write)), sizeof(buffer_write));
    EXPECT_EQ(read(pipe[Pipe::read], buffer_read, sizeof(buffer_read)), sizeof(buffer_read));
}

TEST(GccReaderHelperTest, full_path_of_full_path) {
    EXPECT_EQ(full_path_of("/full/path/to/root", "/some/full/path"), "/some/full/path");
    EXPECT_EQ(full_path_of("/full/path/to/root", "/some/full/path/../other_path"), "/some/full/other_path");
}

TEST(GccReaderHelperTest, full_path_of_relative_path) {
    EXPECT_EQ(full_path_of("/full/path/to/root", "some/relative/path"), "/full/path/to/root/some/relative/path");
    EXPECT_EQ(full_path_of("/full/path/to/root", "some/relative/path/../other_path"), "/full/path/to/root/some/relative/other_path");
    EXPECT_EQ(full_path_of("/full/path/to/root/../other_path", "some/relative/path"), "/full/path/to/other_path/some/relative/path");
}

TEST(GccReaderHelperTest, process_dependency_with_full_paths) {
    const std::vector<std::string> dependency_tree = {
        ". /path/to/file0.h",
        ".. /path/to/file1.h",
        ".. /path/to/file2.h",
        "... /path/to/file3.h",
        ".. /path/to/file4.h",
        ". /path/to/file5.h",
        ".. /path/to/file6.h",
        "... /path/to/file7.h",
        "... /path/to/file8.h",
        ". /path/to/sub_dir/../file9.h",
    };

    const std::string root = "/path/to";
    const std::string root_node = root + "/src/root.cpp";

    const dependency::nodes_T expected_dependency = {
        "/path/to/file0.h",
        "/path/to/file5.h",
        "/path/to/file9.h",
    };

    EXPECT_EQ(process_dependency(root, "/path/to/node.cpp", dependency_tree), expected_dependency);
}

TEST(GccReaderHelperTest, process_dependency_with_files_in_other_tree) {
    const std::vector<std::string> dependency_tree = {
        ". /path/to/file0.h",
        ".. /path/to/file1.h",
        "... /other/path/to/path1.h",
        ".... /other/path/to/path3.h",
        "..... /other/path/to/path3.h",
        "... /other/path/to/path4.h",
        ".. /path/to/file2.h",
        "... /other/path/to/path5.h",
        ".... /other/path/to/path6.h",
        "..... /other/path/to/path7.h",
        "... /path/to/file3.h",
        ".. /path/to/file4.h",
        ". /path/to/file5.h",
        ".. /path/to/file6.h",
        "... /path/to/file7.h",
        "... /path/to/file8.h",
        ". /path/to/file9.h",
        ". /other/path/to/file9.h",
        ". /usr/include/some_header.h",
    };

    const std::string root = "/path/to";
    const std::string root_node = root + "/src/root.cpp";

    const dependency::nodes_T expected_dependency = {
        "/path/to/file0.h",
        "/path/to/file5.h",
        "/path/to/file9.h",
    };

    EXPECT_EQ(process_dependency(root, root_node, dependency_tree), expected_dependency);
}

TEST(GccReaderHelperTest, process_dependency_with_relative_paths) {
    const std::vector<std::string> dependency_tree = {
        ". path/to/file0.h",
        ".. path/to/file1.h",
        ".. path/to/file2.h",
        "... path/to/file3.h",
        ".. path/to/file4.h",
        ". path/to/file5.h",
        ".. path/to/file6.h",
        "... path/to/file7.h",
        "... path/to/file8.h",
        ". path/to/file9.h",
    };

    const std::string root = "/path/to/project/src";
    const std::string root_node = root + "/root.cpp";

    const dependency::nodes_T expected_dependency = {
        root + "/path/to/file0.h",
        root + "/path/to/file5.h",
        root + "/path/to/file9.h",
    };

    EXPECT_EQ(process_dependency(root, root_node, dependency_tree), expected_dependency);
}

TEST(GccReaderHelperTest, read_gcc_output) {
    const std::string stdout_output = "some output to stdout";
    const std::string stderr_output = "some output to stderr";
    Pipe fd_out, fd_err;
    const pid_t pid = ::fork();
    ASSERT_NE(pid, -1);
    if (pid == 0) {
        fd_out.close(Pipe::read);
        fd_err.close(Pipe::read);
        EXPECT_EQ(write(fd_out[Pipe::write], stdout_output.c_str(), stdout_output.size()), stdout_output.size());
        EXPECT_EQ(write(fd_err[Pipe::write], stderr_output.c_str(), stderr_output.size()), stderr_output.size());
        fd_out.close(Pipe::write);
        fd_err.close(Pipe::write);
        std::exit(0);
    }
    fd_out.close(Pipe::write);
    fd_err.close(Pipe::write);
    const auto result = read_gcc_output(fd_out, fd_err, pid);
    EXPECT_EQ(result, stderr_output);
}

TEST(GccReaderHelperTest, read_gcc_output_error) {
    const std::string stderr_output = "error message";
    Pipe fd_out, fd_err;
    const pid_t pid = ::fork();
    ASSERT_NE(pid, -1);
    if (pid == 0) {
        fd_out.close(Pipe::read);
        fd_err.close(Pipe::read);
        EXPECT_EQ(write(fd_err[Pipe::write], stderr_output.c_str(), stderr_output.size()), stderr_output.size());
        fd_out.close(Pipe::write);
        fd_err.close(Pipe::write);
        std::exit(1);
    }
    fd_out.close(Pipe::write);
    fd_err.close(Pipe::write);
    try {
        read_gcc_output(fd_out, fd_err, pid);
        FAIL() << "Expected std::runtime_error to be thrown";
    } catch (const std::runtime_error& err) {
        EXPECT_EQ(err.what(), "gcc exited with status 1 and error message: " + stderr_output);
    } catch (...) {
        FAIL() << "Something other than std::runtime_error was thrown";
    }
}

namespace {
void check_read_gcc_output_stopped_by_signal(int sig) {
    Pipe fd_out, fd_err;
    const pid_t pid = ::fork();
    ASSERT_NE(pid, -1);
    if (pid == 0) {
        fd_out.close(Pipe::read);
        fd_err.close(Pipe::read);
        sleep(5);
        fd_out.close(Pipe::write);
        fd_err.close(Pipe::write);
        std::exit(0);
    }
    fd_out.close(Pipe::write);
    fd_err.close(Pipe::write);
    kill(pid, sig);
    try {
        read_gcc_output(fd_out, fd_err, pid);
        FAIL() << "Expected std::runtime_error to be thrown " << sig;
    } catch (const std::runtime_error& err) {
        EXPECT_EQ(err.what(), "gcc was interrupted by signal " + std::to_string(sig)) << sig;
    } catch (...) {
        FAIL() << "Something other than std::runtime_error was thrown " << sig;
    }
}
}

TEST(GccReaderHelperTest, read_gcc_output_signaled) {
    check_read_gcc_output_stopped_by_signal(SIGTERM);
    check_read_gcc_output_stopped_by_signal(SIGKILL);
}

TEST(GccReaderHelperTest, get_dependency_tree) {
    const std::string err_output = {
        ". /path/to/file0.h\n"
        ".. /path/to/file1.h\n"
        ".. /path/to/file2.h\n"
        "... /path/to/file3.h\n"
        ".. /path/to/file4.h\n"
        ". /path/to/file5.h\n"
        ".. /path/to/file6.h\n"
        "... /path/to/file7.h\n"
        "... /path/to/file8.h\n"
        ". /path/to/file9.h\n"
        "Multiple include guards may be useful for:\n"
        "/usr/include/x86_64-linux-gnu/bits/long-double.h\n"
        "/usr/include/x86_64-linux-gnu/bits/wordsize.h\n"
        "/usr/include/x86_64-linux-gnu/gnu/stubs-64.h\n"
        "/usr/include/x86_64-linux-gnu/gnu/stubs.h\n"
        "/usr/lib/gcc/x86_64-linux-gnu/8/include/stddef.h\n"

    };
    const std::vector<std::string> expected_tree = {
        ". /path/to/file0.h",
        ".. /path/to/file1.h",
        ".. /path/to/file2.h",
        "... /path/to/file3.h",
        ".. /path/to/file4.h",
        ". /path/to/file5.h",
        ".. /path/to/file6.h",
        "... /path/to/file7.h",
        "... /path/to/file8.h",
        ". /path/to/file9.h",
    };

    EXPECT_EQ(get_dependency_tree_from(err_output), expected_tree);
}

TEST(GccReaderHelperTest, process_file_for_file1_3_cpp) {
    const std::string files_root = PROJECT_DIR "/test/files/src";
    const std::string file = files_root + "/dir1/file1_3.cpp";
    const std::string options = "-std=c++17 -I " + files_root;

    const dependency::nodes_T expected_dependency = {
        files_root + "/dir1/file1_1.h",
        files_root + "/dir1/file1_2.h",
    };

    EXPECT_EQ(process_file(files_root, file, options), expected_dependency);
}

TEST(GccReaderHelperTest, process_file_for_file1_2_h) {
    const std::string files_root = PROJECT_DIR "/test/files/src";
    const std::string file = files_root + "/dir1/file1_2.h";
    const std::string options = "-std=c++17 -I " + files_root;

    const dependency::nodes_T expected_dependency = {
        files_root + "/dir2/file2_1.h",
    };

    EXPECT_EQ(process_file(files_root, file, options), expected_dependency);
}

}
}
