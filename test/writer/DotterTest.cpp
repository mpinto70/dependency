#include <writer/Dotter.h>

#include <gtest/gtest.h>
#include <iostream>

namespace writer {
using dependency::DependencyGraph;
using dependency::Writer;

TEST(DotterTest, write) {
    DependencyGraph graph;
    graph.add("a/b/c", { "b/c/d" });
    graph.add("c/d/e", { "d/e/f" });
    graph.add("e/f/g", { "f/g/h" });

    const std::string expected =
          "digraph g\n"
          "{\n"
          "    rankdir=LR;\n"
          "    node [ shape=record, fontname=Helvetica ];\n"
          "\n"
          "    // ========Nodes========\n"
          "    a_b_c [ label=\"a/b\\nc\" fontcolor=\"color1\", color=\"color1\", style=filled, fillcolor=\"color2\" ];\n"
          "    b_c_d [ label=\"b/c\\nd\" ];\n"
          "    c_d_e [ label=\"c/d\\ne\" ];\n"
          "    d_e_f [ label=\"d/e\\nf\" fontcolor=\"color3\", color=\"color3\", style=filled, fillcolor=\"color4\" ];\n"
          "    e_f_g [ label=\"e/f\\ng\" fontcolor=\"color3\", color=\"color3\", style=filled, fillcolor=\"color4\" ];\n"
          "    f_g_h [ label=\"f/g\\nh\" ];\n"
          "\n"
          "    // ========Dependencies========\n"
          "    a_b_c -> b_c_d;\n"
          "\n"
          "    c_d_e -> d_e_f;\n"
          "\n"
          "    e_f_g -> f_g_h;\n"
          "\n"
          "}\n";

    Dotter dotter;
    Writer& writer = dotter;

    const auto highlights = dependency::Highlight::create(std::vector<std::string>{ "a/b,color1,color2", "e/f,color3,color4" });
    std::ostringstream out;
    writer.write(out, graph, highlights);
    EXPECT_EQ(out.str(), expected);
}

TEST(DotterTest, write_with_auto_highlight) {
    DependencyGraph graph;
    graph.add("a/b/c", { "b/c/d" });
    graph.add("c/d/e", { "d/e/f" });
    graph.add("e/f/g", { "f/g/h" });

    const std::string expected =
          "digraph g\n"
          "{\n"
          "    rankdir=LR;\n"
          "    node [ shape=record, fontname=Helvetica ];\n"
          "\n"
          "    // ========Nodes========\n"
          "    a_b_c [ label=\"a/b\\nc\" fontcolor=\"color1\", color=\"color1\", style=filled, fillcolor=\"color2\" ];\n"
          "    b_c_d [ label=\"b/c\\nd\" ];\n"
          "    c_d_e [ label=\"c/d\\ne\" ];\n"
          "    d_e_f [ label=\"d/e\\nf\" fontcolor=\"color3\", color=\"color3\", style=filled, fillcolor=\"color4\" ];\n"
          "    e_f_g [ label=\"e/f\\ng\" fontcolor=\"color3\", color=\"color3\", style=filled, fillcolor=\"color4\" ];\n"
          "    f_g_h [ label=\"f/g\\nh\" ];\n"
          "\n"
          "    // ========Dependencies========\n"
          "    a_b_c -> b_c_d;\n"
          "\n"
          "    c_d_e -> d_e_f;\n"
          "\n"
          "    e_f_g -> f_g_h;\n"
          "\n"
          "}\n";

    Dotter dotter;
    Writer& writer = dotter;

    std::ostringstream out;
    const auto highlights = dependency::Highlight::create(std::vector<std::string>{ "a/b,color1,color2", "e/f,color3,color4" });
    writer.write(out, graph, highlights);
    EXPECT_EQ(out.str(), expected);
}
}
